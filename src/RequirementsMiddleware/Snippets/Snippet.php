<?php

namespace Solnet\RequirementsMiddleware\Snippets;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\SiteConfig\SiteConfig;

/**
 * Represents a JavaScript snippet that is inserted into
 * templates according to particular rules.
 */

class Snippet extends DataObject
{
    private static $singular_name = 'Snippet';
    private static $plural_name = 'Snippets';
    private static $table_name = 'RequirementsMiddleware_Snippet';

    private static $db = [
        'Title' => 'Varchar',
        'Code' => 'Text',
        'Element' => 'Varchar',
        'Position' => 'Enum("Top,Bottom","Top")',
        'IsEnabled' => 'Boolean',
        'Sort' => 'Int',
    ];

    private static $has_one = [
        'SiteConfig' => SiteConfig::class,
    ];

    /**
     * @config
     * Available options for elements to detect in the output.
     * css selector => CMS label
     */
    private static $available_elements = [
        'head' => '<head>',
        'body' => '<body>',
    ];

    private static $summary_fields = [
        'Title' => 'Title',
        'ElementNice' => 'Element',
        'Position' => 'Position',
    ];

    /**
     * Enable extensions to add extra controls on whether each item is enabled or not.
     *
     * @return boolean
     */
    public function checkIsEnabled()
    {
        $isEnabled = $this->IsEnabled;
        $this->extend('updateIsEnabled', $isEnabled);
        return !!$isEnabled;
    }

    /**
     * Outputs the 'Nice' view of the selected element for this snippet - used in gridfields
     *
     * @return string
     */
    public function ElementNice()
    {
        $availableElements = $this->config()->available_elements;
        if (!$this->Element || !is_array($availableElements) || !array_key_exists($this->Element, $availableElements)) {
            return '(None)';
        }
        return $availableElements[$this->Element];
    }

    /**
     * Return the CMS editing interface for a Snippet.
     *
     * @return SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName(['Sort', 'SiteConfigID']);

        $fields->dataFieldByName('Title')
        ->setTitle(_t('RequirementsMiddleware.Snippet_Title_Title', 'Title'))
        ->setDescription(
            _t(
                'RequirementsMiddleware.Snippet_Title_Description',
                'For CMS use only, does not appear on site.'
            )
        );

        $fields->dataFieldByName('Code')
        ->setTitle(_t('RequirementsMiddleware.Snippet_Code_Title', 'Code'))
        ->setDescription(
            _t(
                'RequirementsMiddleware.Snippet_Code_Description',
                'Enter the code snippet you wish to add to the site, including any &lt;script&gt; tags.'
            )
        );

        $fields->replaceField(
            'Element',
            DropdownField::create(
                'Element',
                _t('RequirementsMiddleware.Snippet_Element_Title', 'Element'),
                $this->config()->available_elements
            )
        );

        return $fields;
    }
}
