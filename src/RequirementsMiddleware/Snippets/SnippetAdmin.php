<?php

namespace Solnet\RequirementsMiddleware\Snippets;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\ORM\DataExtension;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

/**
 * Adds Snippet administration to SiteConfig, allowing users to manage the JavaScript snippets available for the site.
 */

class SnippetAdmin extends DataExtension
{
    private static $has_many = [
        'Snippets' => Snippet::class,
    ];

    /**
     * @config
     * Sets the tab the config will be added to
     */
    private static $cms_tab = 'Root.CodeSnippets';

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab(
            $this->owner->config()->cms_tab,
            [
                GridField::create(
                    'Snippets',
                    _t('RequirementsMiddleware.SnippetAdmin_Snippets_Title', 'Code snippets'),
                    $this->owner->Snippets(),
                    GridFieldConfig_RelationEditor::create()->addComponent(
                        new GridFieldOrderableRows('Sort')
                    )
                ),
            ]
        );
    }
}
