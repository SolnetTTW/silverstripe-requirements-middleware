<?php

namespace Solnet\RequirementsMiddleware;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Control\Middleware\HTTPMiddleware;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\View\SSViewer;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * Inserts code snippets into the final output of HTML type responses.
 */

class RequirementsMiddleware implements HTTPMiddleware
{
    use Configurable;

    /**
     * @config
     * List of themes we don't want to add snippets to
     */
    private static $themes_blacklist = [
        'silverstripe/admin:cms-forms',
    ];

    public function process(HTTPRequest $request, callable $delegate)
    {
        $response = $delegate($request);

        // Don't process dev/xxx URLs
        // Particularly, CWP deploy process runs dev and then dev/check before dev/build.
        // If columns have changed on SiteConfig or Snippet, this causes a SQL error.
        if ($request->getURL() !== 'dev' && substr($request->getURL(), 0, 4) !== 'dev/') {
            // Add HTML snippets into output
            $config = SiteConfig::current_site_config();
            foreach ($config->Snippets() as $snippet) {
                if (!$snippet->checkIsEnabled()) {
                    continue;
                }
                // Check this is HTML output
                $headers = $response->getHeaders();
                if (!array_key_exists('content-type', $headers)) {
                    continue;
                }
                if (strpos($headers['content-type'], 'text/html') === -1) {
                    continue;
                }
                // Check we're not using the CMS theme - we don't want to
                // output stuff in the CMS
                $themes = SSViewer::get_themes();
                $themesBlacklist = $this->config()->themes_blacklist;
                if (count(array_intersect($themes, $themesBlacklist)) > 0) {
                    continue;
                }
                // Put the snippet into the output if possible
                $dom = HtmlDomParser::str_get_html($response->getBody());
                if ($dom) {
                    $element = $dom->find($snippet->Element);
                    if ($element && count($element) > 0) {
                        if ($snippet->Position == 'Top') {
                            $element[0]->innertext = $snippet->Code . $element[0]->innertext;
                        } else {
                            $element[0]->innertext = $element[0]->innertext . $snippet->Code;
                        }
                    }
                    $response->setBody($dom->save());
                }
            }
        }

        return $response;
    }
}
